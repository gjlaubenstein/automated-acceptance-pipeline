package com.dogland.dog;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DogController {

    @Autowired
    private DogRepository dogRepository;

    @PostMapping("/dogs")
    @ResponseStatus(HttpStatus.CREATED)
    public Dog create(@RequestBody Dog request) {
        Dog dog = dogRepository.save(request);
        return dog;
    }
}
