Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: scenario description
Given a new dog named Xander
When an API user creates the dog
Then the dog is saved for future record keeping
