package integration;

import com.dogland.dog.DogApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DogApplication.class})
public class TestIntegrationTest {
    @Test
    public void contextLoads() {
    }
}
