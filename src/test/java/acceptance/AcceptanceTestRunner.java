package acceptance;

import acceptance.steps.AddDogSteps;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.io.LoadFromURL;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.CrossReference;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.core.steps.SilentStepMonitor;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Collections;
import java.util.List;

import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

@RunWith(JUnit4.class)
public class AcceptanceTestRunner extends JUnitStories {

    public AcceptanceTestRunner() {
        configure();
    }

    private void configure() {
        Class<?> thisClass = this.getClass();
        useConfiguration(new MostUsefulConfiguration()
                .useStoryLoader(new LoadFromURL())
                .usePendingStepStrategy(new FailingUponPendingStep())
                .useStoryReporterBuilder(new StoryReporterBuilder()
                        .withCodeLocation(codeLocationFromClass(thisClass))
                        .withDefaultFormats()
                        .withFormats(Format.CONSOLE, Format.TXT)
                        .withCrossReference(new CrossReference())
                        .withFailureTrace(true))
                .useStepMonitor(new SilentStepMonitor()));
    }

    @Override
    public InstanceStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new AddDogSteps());
    }
    protected List<String> storyPaths() {
        String storiesUrl = this.getClass().getResource("/stories").toString();
        String storiesPath = this.getClass().getResource("/stories").getFile();
        List<String> includePaths = Collections.singletonList("*.story");
        List<String> excludePaths = Collections.singletonList("exclude*.story");
        return new StoryFinder().findPaths(storiesPath, includePaths, excludePaths, storiesUrl + "/");
    }
}
