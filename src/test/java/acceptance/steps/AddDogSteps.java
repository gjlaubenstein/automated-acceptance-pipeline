package acceptance.steps;

import acceptance.DogApi;
import com.dogland.dog.Dog;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class AddDogSteps {

    private String dogName;
    private Dog dog;

    @Given("a new dog named $name")
    public void givenANewDogNamedXander(@Named("name") String name) {
        dogName = name;
    }

    @When("an API user creates the dog")
    public void whenAnAPIUserCreatesTheDog() {
        DogApi dogApiClient = Feign.builder()
                .decoder(new JacksonDecoder())
                .encoder(new JacksonEncoder())
                .target(DogApi.class, System.getenv("HOST"));

        dog = dogApiClient.createDog(dogName);
    }

    @Then("the dog is saved for future record keeping")
    public void thenTheDogIsSavedForFutureRecordKeeping() {
        Assert.assertEquals(dogName, dog.getName());
    }
}
