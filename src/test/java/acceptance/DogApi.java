package acceptance;

import com.dogland.dog.Dog;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface DogApi {
    @RequestLine("POST /dogs")
    @Headers("Content-Type: application/json")
    Dog createDog(@Param("name") String name);
}
