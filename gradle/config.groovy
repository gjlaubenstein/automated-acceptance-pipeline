environments {
    local {
    }

    test {
    }

    dev {
        cf {
            target = "api.run.pivotal.io"
            space = "development"
            org = "rhdevshop"
            appName = "dogs-api-dev"
            instances = 1
            memory = 512
        }
    }

    uat {
        cf {
            target = "api.run.pivotal.io"
            space = "uat"
            org = "rhdevshop"
            appName = "dogs-api-uat"
            instances = 1
            memory = 512
        }
    }

    sandbox {
        cf {
            target = "api.run.pivotal.io"
            space = "sandbox"
            org = "rhdevshop"
            appName = "dogs-api-sand"
            instances = 1
            memory = 512
        }
    }

    production {
        cf {
            target = "api.run.pivotal.io"
            space = "prod"
            org = "rhdevshop"
            appName = "dogs-api-prod"
            instances = 1
            memory = 512
        }
    }
}
